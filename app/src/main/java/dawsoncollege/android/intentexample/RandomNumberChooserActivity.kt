package dawsoncollege.android.intentexample

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import dawsoncollege.android.intentexample.databinding.ActivityRandomNumberChooserBinding

private const val RANDOM_ORG_URL =
    "https://www.random.org/integers/?num=100&min=1&col=5&base=10&format=html"

class RandomNumberChooserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRandomNumberChooserBinding

    companion object {
        private const val LOG_TAG = "RN_CHOOSER_DEV_LOG"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRandomNumberChooserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.openWebsiteBtn.setOnClickListener {
            val maximumNumberStr = binding.maximumNumberEditTxt.text.toString()
            Log.d(LOG_TAG, "maxNumInputStr is '$maximumNumberStr', before opening Website")
            // TODO : maxNumInput validation...

            val urlWithMaximumNumber = "$RANDOM_ORG_URL&max=$maximumNumberStr"

            // implicit intent
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlWithMaximumNumber))

            try {
                Log.d(LOG_TAG, "Visiting '$urlWithMaximumNumber'")
                // firing the implicit intent
                startActivity(intent)
            } catch (exc: ActivityNotFoundException) {
                Log.e(LOG_TAG, "Could not open the website", exc)
            }
        }

        // Everytime the input in maximumNumberEditTxt changes, we update what will eventually be
        // returned to the parent activity (MainActivity). When this activity is finished, the
        // parent activity will only receive the latest bundle of data
        binding.maximumNumberEditTxt.addTextChangedListener {
            val maximumNumberStr = it.toString()
            Log.d(LOG_TAG, "maxNumInputStr has changed to '$maximumNumberStr'")

            val bundle = Bundle()
            bundle.putString(MainActivity.MAXIMUM_NUMBER_BUNDLE_KEY, maximumNumberStr)

            // https://kotlinlang.org/docs/scope-functions.html
            setResult(Activity.RESULT_OK, Intent().apply { putExtras(bundle) })
        }
    }
}