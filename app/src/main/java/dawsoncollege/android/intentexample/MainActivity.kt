package dawsoncollege.android.intentexample

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import dawsoncollege.android.intentexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    // https://kotlinlang.org/docs/object-declarations.html#companion-objects
    companion object {
        private const val LOG_TAG = "MAIN_ACTIVITY_DEV_LOG"
        private const val RN_CHOOSER_REQUEST_CODE = 0
        const val MAXIMUM_NUMBER_BUNDLE_KEY = "MAXIMUM_NUMBER_KEY"
    }

    // Part (1/2)
    // This is the new way to do things since AndroidX 1.2.0a02. FYI only
    //
    // Here is the doc for the release introducing it:
    // https://developer.android.com/jetpack/androidx/releases/activity#1.2.0-alpha02
    //
    // Here is a nice Android developer guide for it:
    // https://developer.android.com/training/basics/intents/result
    //
    // And here is a good article about it:
    // https://medium.com/droid-log/androidx-activity-result-apis-the-new-way-7cfc949a803c
    //
    // It handles the requestCode stuff automatically,
    // but works with Intent objects under the hood
//    private val randomNumberChooserActivityLauncher =
//        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
//            val maximumNumberStr: String? =
//                activityResult.data?.extras?.getString(MAXIMUM_NUMBER_BUNDLE_KEY)
//
//            Toast.makeText(
//                this,
//                if (maximumNumberStr != null) "Maximum number was : $maximumNumberStr"
//                else "Could not get maximum number...",
//                Toast.LENGTH_LONG
//            ).show()
//        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.openSecondActivityBtn.setOnClickListener {
            val intent = Intent(this, RandomNumberChooserActivity::class.java)

            try {
                // firing the explicit intent, comment this if you want to try the new part
                startActivityForResult(intent, RN_CHOOSER_REQUEST_CODE)

                // Part (2/2)
                // This is the new way to do things since AndroidX 1.2.0a02. FYI only
//                randomNumberChooserActivityLauncher.launch(intent)
            } catch (exc: ActivityNotFoundException) {
                Log.e(LOG_TAG, "Could not open RandomNumberChooserActivity", exc)
            }
        }
    }

    // comment this function if you want to try the new part
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /*
         * `requestCode` is used to distinguish which Intent we are returning from.
         * E.g.
         * if `MainActivity` was sending 2 intents to different activities, `onActivityResult()`
         * needs to know which we are returning from in order to handle the returned data correctly
         *
         * `resultCode` is used to check if the returning activity with success
         *
         * `data` is the intent object that was passed to `setResult()` in `RandomActivity`, it's
         * what we get back from the child activity.
         */

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                RN_CHOOSER_REQUEST_CODE -> {
                    Log.d(LOG_TAG, "Returning from RandomNumberChooserActivity successfully")
                    val maximumNumberStr: String? =
                        data?.extras?.getString(MAXIMUM_NUMBER_BUNDLE_KEY)

                    Toast.makeText(
                        this,
                        if (maximumNumberStr != null) "Maximum number was : $maximumNumberStr"
                        else "Could not get maximum number...",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else -> {
                    Log.w(LOG_TAG, "Returning from an unknown activity")
                }
            }
        } else {
            Log.w(LOG_TAG, "Activity result was not 'Activity.RESULT_OK' (-1), was '$resultCode'")
        }
    }
}